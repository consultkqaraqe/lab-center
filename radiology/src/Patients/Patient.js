import React, {Component} from 'react';
import {Table} from 'react-bootstrap';
import './Patients.css';
class Patients extends Component {
    constructor(props) {
        super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
        this.state = { //state is by default an object
           patients: [
              { id: 1, name: 'Wasif', birthDate: '21/9/1999', gender: 'male', idNumber: 123456789, phone: '0599999999', email: 'wasif@email.com', address: 'bethlehem' },
              { id: 2, name: 'Ali', birthDate: '19/5/2000', gender: 'male', idNumber: 123456780, phone: '0599999991', email: 'ali@email.com', address: 'hebron' },
              { id: 3, name: 'Saad', birthDate: '16/2/1980', gender: 'male', idNumber: 123456781, phone: '0599999992',email: 'saad@email.com', address: 'nablus'},
              { id: 4, name: 'Asada', birthDate: '25/3/1998', gender: 'female', idNumber: 123456782, phone: '0599999993', email: 'asada@email.com', address: 'ramallah' }
           ]
        }
     }



     renderTableData() {
        return this.state.patients.map((patient, index) => {
           const { id, name, birthDate, gender, phone, address, idNumber } = patient //destructuring
           return (
              <tr key={id}>
                 <td><button id='savePatient'>تحديث</button></td>
                 <td><input type="text" value={address} /> </td>
                 <td><input type="date" value={birthDate} /></td>
                 <td><input type="text" value={gender} /></td>
                 <td><input type="text" value={idNumber} /></td>
                 <td><input type="text" value={phone} /></td>
                 <td><input type="text" value={name} /></td>
              </tr>
           )
        })
     }

    render() {
        return (
            <div className='container'>
                <Table id='patients'>
                    <thead>
                        <th></th>
                        <th>العنوان</th>
                        <th>تاريخ الميلاد</th>
                        <th>الجنس</th>
                        <th>رقم الهوية</th>
                        <th>رقم الجوال</th>
                        <th>الإسم</th>
                    </thead>
                        
               <tbody>
                  {this.renderTableData()}
               </tbody>
                
                </Table>
            </div>
        )
    }
}

export default Patients;
